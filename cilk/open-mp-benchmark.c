#include <omp.h>
#include "benchmark.h"

static double start_time = 0;
static double end_time   = 0;

void benchmark_set_up(void) 
{
	start_time = omp_get_wtime();
}

void benchmark_tear_down(void)
{
	end_time = omp_get_wtime();
}

double benchmark_get_runtime(void)
{
	return end_time - start_time;
}
