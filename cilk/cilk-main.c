#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <cilk/cilk_api.h>
#include "prefixsum.h"
#include "types.h"
#include "input-generator.h"
#include "util.h"
#include "operation.h"
#include "benchmark.h"

/** The generic program name */
#define PROG_NAME "prefixsums"

/** The programs arguments */
extern struct arguments args;

/** Prints the programs usage. */
void usage(void);

/**
 * Parses the programs arguments.
 *
 * @param argc the argument counter from the command line
 * @param argv the argument vector from the command line
 **/
void parse_arguments(int argc, char **argv);

/** Runs the parallel solutions benchmarks. */
void run_benchmarks(void);

char buffer[10];

int main(int argc, char **argv)
{
	args.program_name = PROG_NAME;
	
	srand(time(NULL));
	
	parse_arguments(argc, argv);
	
	snprintf(buffer, 10, "%lu", args.number_cores); 
	
	#ifndef SEQUENTIAL
	if (0 != __cilkrts_set_param("nworkers",buffer)){
		bail_out(EXIT_FAILURE, "Failed to set worker count");
	}
	#endif
	
	run_benchmarks();
	
	return EXIT_SUCCESS;
}

void run_benchmarks(void)
{
	int i;
	
	for (i = 0; i < args.number_repetitions; i++) {
	
		Type *problem = generate_random_problem(args.problem_size);
		Type *copy = copy_array(problem, args.problem_size);
	
		benchmark_set_up();
	
		prefixsum(problem, args.problem_size, operation);
	
		benchmark_tear_down();
		
		seq_benchmark_set_up();
		sequential_prefixsum(copy, args.problem_size, operation);
		seq_benchmark_tear_down();
		if (!compare_arrays(problem, copy, args.problem_size)) {
			bail_out(EXIT_FAILURE, "Error detected an incorrect result\n");
		}
		free(copy);
		free(problem);
	}
	
	print_performance_summary(args.problem_size, args.number_cores,
			benchmark_avg_runtime(), benchmark_min_runtime(), 
			benchmark_max_runtime(), get_plus_count(), 
			sequential_avg_runtime());
}

void parse_arguments(int argc, char **argv)
{
	char *endptr = NULL;
	
	if (argc != 4) {
		usage();
		bail_out(EXIT_FAILURE, "invalid command line arguments");
	}
	
	args.problem_size = strtol(argv[1], &endptr, 10);
	
	if (args.problem_size == LONG_MIN || args.problem_size == LONG_MAX
			|| args.problem_size < 0) {
		bail_out(EXIT_FAILURE, "invalid problem size");
	}
	
	args.number_cores = strtol(argv[2], &endptr, 10);
	
	if (args.number_cores == LONG_MIN || args.number_cores == LONG_MAX
			|| args.number_cores < 0) {
		bail_out(EXIT_FAILURE, "invalid number of cores");
	}
	
	args.number_repetitions = strtol(argv[3], &endptr, 10);
	
	if (args.number_repetitions == LONG_MIN 
			|| args.number_repetitions == LONG_MAX 
			|| args.number_repetitions < 0) {
		bail_out(EXIT_FAILURE, "invalid number of repetitions");
	}
}

void usage() 
{
	(void) fprintf(stdout, "usage: %s <problem-size> <number-of-cores> "
			"<number-repetitions>\n", 
			args.program_name);
}
