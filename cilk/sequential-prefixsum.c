#include "prefixsum.h"
#include "matrix.h"

void prefixsum(Type * x, int n, Type (*f)(Type * e1, Type * e2))
{
	Type sum;
	long i;
	
	if (n <= 1) return;
	
	sum = x[0];
	
	for (i = 1; i < n; i++) {
		sum = f(&sum, &x[i]); x[i] = sum;
	}
}
