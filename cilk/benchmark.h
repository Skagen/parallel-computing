#ifndef DEF_BENCHMARK_H
#define DEF_BENCHMARK_H

/** Begins a benchmark for the parallel program */
void benchmark_set_up(void);

/** Ends a benchmark of the parallel program */
void benchmark_tear_down(void);

/** @return the average runtime of the parallel solution */
double benchmark_avg_runtime(void);

/** @return the maximum runtime of the parallel solution */
double benchmark_max_runtime(void);

/** @return the minimum runtime of the parallel solution */
double benchmark_min_runtime(void);

/** @return the average runtime of the sequential solution */
double sequential_avg_runtime(void);

/** Begins a benchmark for the sequential solution. */
void seq_benchmark_set_up(void);

/** Ends a benchmark of the sequential solution. */
void seq_benchmark_tear_down(void);

#endif
