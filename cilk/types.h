#ifndef DEF_TYPES_H
#define DEF_TYPES_H

#ifndef MATRIX_SIZE
	/** the value to use as width and height for the matrices */
	#define MATRIX_SIZE 1
#endif

#include "matrix.h"

#endif
