#include <stdlib.h>
#include <stdio.h>
#include <cilk/cilk.h>
#include "util.h"
#include "operation.h"
#include "prefixsum.h"

extern struct arguments args;

#define UNIT ((n/args.number_cores < 1 ? 1 : n/args.number_cores))

void reduce(int i, void * params);
void map(int i, void * params);
void cilk_parallel_for(int i, int j, int unit, void * params,
		void (*task)(int i, void * params));

struct parameters {
	Type * x;
	Type * y;
	Type (*f)(Type *e1, Type *e2);
};

void prefixsum(Type *x, int n, Type (*f)(Type *e1, Type *e2)) 
{
	Type * y = NULL;
	struct parameters params;
	
	if (n <= 1) {
		return;
	}
	
	y = malloc((n/2) * sizeof(Type));
	if (y == NULL) {
		bail_out(EXIT_FAILURE, "failed to allocate memory");
	}
	
	params.x = x;
	params.y = y;
	params.f = f;
	
	cilk_parallel_for(0, n/2 - 1, UNIT, &params, reduce);
	
	prefixsum(y, n/2, f);
	
	x[1] = y[0];
	cilk_parallel_for(1, n/2 - 1, UNIT, &params, map);
	
	if (n % 2 == 1) x[n-1] = f(&y[n/2 - 1],&x[n-1]);
	
	free(y);
}

void reduce(int i, void * params) 
{
	Type * x = ((struct parameters *) params)->x;
	Type * y = ((struct parameters *) params)->y;
	Type (*f)(Type *e1, Type *e2) = ((struct parameters *) params)->f;
	y[i] = f(&x[2*i],&x[2*i + 1]);
}

void map(int i, void * params)
{
	Type * x = ((struct parameters *) params)->x;
	Type * y = ((struct parameters *) params)->y;
	Type (*f)(Type *e1, Type *e2) = ((struct parameters *) params)->f;
	x[2*i] = f(&y[i-1], &x[2*i]);
	x[2*i + 1] = y[i];
}

void cilk_parallel_for(int i, int j, int unit, void * params,
		void (*task)(int i, void * params))
{
	int k;

	if (j - i <= unit) {
		for (k = i; k <= j; k++) task(k, params);
	} else {
		cilk_spawn cilk_parallel_for(i, (j+i)/2, unit, params, task);
		cilk_spawn cilk_parallel_for((j+i)/2 + 1, j, unit, params, task);
		cilk_sync;
	}
}
