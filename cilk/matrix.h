#ifndef DEF_MATRIX_H
#define DEF_MATRIX_H

#include "types.h"

/** Defines square matrix with width and height equal to MATRIX_SIZE. */
typedef struct {
	int a[MATRIX_SIZE][MATRIX_SIZE];
} Matrix;

#endif
