function run_command
{
	# 1:program 2:problem 3:cores 4:repetitions
	local result=""
	
	if [ $3 -le 16 ]
	then
		result=`mpirun -node 0 -nnp $3 ./$1 $2 $3 $4`
	else
		local nodes=$(($3 / 16))
		result=`mpirun -nn $nodes -nnp 16 ./$1 $2 $3 $4`
	fi
	
	echo "$result"
}
