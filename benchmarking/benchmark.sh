#!/bin/bash

host=$1
project=$2
program=$3
benchmark=$4

source benchmarks.sh

case "$project" in

	open-mp)	dir="open-mp/"
				source $host/common-command-runner.sh
				source $host/common-test-data.sh
				;;
				
	cilk)		dir="cilk/"
				source $host/common-command-runner.sh
				source $host/common-test-data.sh
				;;
				
	mpi)		dir="mpi/"
				source $host/mpi-environment.sh
				source $host/mpi-command-runner.sh
				source $host/mpi-test-data.sh
				;;
		
	*)			echo "unknown project"
				exit

esac

cd "../$dir"

make clean-all

case "$benchmark" in

	counters)	counter_benchmark
				;;
				
	problems)	integer_problem_size_benchmark
				matrix_problem_size_benchmark
				;;
				
	cores)		integer_number_cores_benchmark
				matrix_number_cores_benchmark
				;;
				
	*)			echo "unknown benchmark"

esac

make clean-all

cd ..
