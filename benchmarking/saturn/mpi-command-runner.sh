function run_command
{
	# 1:program 2:problem 3:cores 4:repetitions
	local result="`mpirun -np $3 ./$1 $2 $3 $4`"
	echo "$result"
}
