function counter_benchmark 
{
	local output_dir="stats/counters/"
	local header="problem cores avg min max operations seq"
	
	mkdir -p $output_dir
	
	make clean-all
	make $program 'TYPE=int' 'COUNTERS=enable'
	echo "$header" > $output_dir/$program.output
	for problem in "${ct_problems[@]}"
	do
		for core in "${ct_cores[@]}"
		do
			echo "counter benchmark program($program) cores($core) problem($problem)"
			echo $(run_command $program $problem $core "1") \
					>> $output_dir/$program.output
		done
	done
}

function integer_problem_size_benchmark
{
	local output_dir="stats/problem-size/integer/"
	local header="problem cores avg min max operations seq"
	
	mkdir -p $output_dir
	
	make clean-all
	make $program 'TYPE=int' 'CORRECTNESS=enable'
	echo "$header" > $output_dir/$program.output
	for core in "${pb_cores[@]}"
	do
		for problem in "${pb_problems[@]}"
		do
			echo "problem benchmark integer program($program) cores($core) problem($problem)"
			echo $(run_command $program $problem $core $repetitions) \
					>> $output_dir/$program.output
		done
	done
}

function matrix_problem_size_benchmark
{
	local output_dir="stats/problem-size/matrix/"
	local header="problem cores avg min max operations seq matrix"
	mkdir -p $output_dir
		
	echo "$header" > $output_dir/$program.output
	
	for matrix_size in "${matrix_sizes[@]}"
	do
		make clean-all
		make $program 'TYPE=Matrix' "MATRIX_SIZE=$matrix_size" 'CORRECTNESS=enable'
		for core in "${pb_cores[@]}"
		do
			for problem in "${pb_problems[@]}"
			do
				echo "problem benchmark matrix($matrix_size) program($program) cores($core) problem($problem)"
				echo $(run_command $program $problem $core $repetitions) $matrix_size \
						>> "$output_dir/$program.output"
			done
		done
	done
}

function integer_number_cores_benchmark
{
	local output_dir="stats/number-cores/integer/"
	local header="problem cores avg min max operations seq"
	
	mkdir -p $output_dir
	
	make clean-all
	make $program 'TYPE=int' 'CORRECTNESS=enable'
	echo "$header" > $output_dir/$program.output
	for core in "${co_cores[@]}"
	do
		for problem in "${co_problems[@]}"
		do
			echo "core benchmark integer program($program) cores($core) problem($problem)"
			echo $(run_command $program $problem $core $repetitions) \
					>> $output_dir/$program.output
		done
	done
}

function matrix_number_cores_benchmark
{
	local output_dir="stats/number-cores/matrix/"
	local header="problem cores avg min max operations seq matrix"
	mkdir -p $output_dir
		
	echo "$header" > $output_dir/$program.output
	
	for matrix_size in "${matrix_sizes[@]}"
	do
		make clean-all
		make $program 'TYPE=Matrix' "MATRIX_SIZE=$matrix_size" 'CORRECTNESS=enable'
		for core in "${co_cores[@]}"
		do
			for problem in "${co_problems[@]}"
			do
				echo "core benchmark matrix($matrix_size) program($program) cores($core) problem($problem)"
				echo $(run_command $program $problem $core $repetitions) $matrix_size \
						>> "$output_dir/$program.output"
			done
		done
	done
}
