#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include "mpi-util.h"
#include "comparator.h"

struct arguments args;

void sequential_prefixsum(Type * x, long n, Type (*f)(Type * e1, Type * e2))
{
	Type sum;
	long i;
	
	if (n <= 1) return;
	
	sum = x[0];
	
	for (i = 1; i < n; i++) {
		sum = f(&sum, &x[i]); x[i] = sum;
	}
}

Type * copy_array(Type * x, long n)
{
	int i;
	Type * y = (Type *) malloc(n * sizeof(Type));
	
	if (y == NULL) {
		bail_out(EXIT_FAILURE, "failed to copy array");
	}
	 
	for (i = 0; i < n; i++) {
		y[i] = x[i];
	}
	
	return y;
}

int compare_arrays(Type * x, Type * y, long n)
{
	long i;
	for (i = 0; i < n; i++) {
		if (!compare_elements(&x[i],&y[i])) {
			return 0;
		}
	}
	return 1;
}

void print_int_array(int * x, int n) {
	int i;
	for (i = 0; i < n; i++) {
		printf("%d ", x[i]);
	}
	printf("\n");
}

void print_matrix_array(Matrix * x, int n) {
	int k, i, j;
	for (i = 0; i < MATRIX_SIZE; i++) {
		for (k = 0; k < n; k++) {
			for (j = 0; j < MATRIX_SIZE; j++) {
				(void) fprintf(stdout, "%d", x[k].a[i][j]);
			}
			(void) fprintf(stdout, " ");
		}
		(void) fprintf(stdout, "\n");
	}
}

void print_performance_summary(long problem_size, long number_cores, 
		double avg_time, double min_time, double max_time, long number_plus, 
		double avg_seq_time)
{
	(void) fprintf(stdout, "%lu %lu %f %f %f %lu %f\n", problem_size, 
			number_cores, avg_time, min_time, max_time, number_plus, avg_seq_time);
}

void bail_out(int retval, const char *fmt, ...) 
{	
	va_list ap;

	(void) fprintf(stderr, "%s : ", args.program_name);

	va_start(ap, fmt);	
	(void) vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (errno != 0) {
		(void) fprintf(stderr, " : %s", strerror(errno));
	}

	(void) fputc('\n', stderr);
	
	exit(retval);
}
