#include "operation.h"

static long plus_counter = 0;

static int disable = 0;

int int_addition(int * n1, int * n2);

int (*operation)(int *a, int * b) = &int_addition;

int int_addition(int * n1, int * n2)
{
	#ifdef ENABLE_COUNTERS
	if (!disable) {
		plus_counter++;
	}
	#endif
	return *n1 + *n2;
}

long get_plus_count(void) 
{
	return plus_counter;
}

void disable_counters(void)
{
	disable = 1;
}
