#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include "input-generator.h"
#include "operation.h"
#include "mpi-prefixsum.h"
#include "mpi-benchmark.h"
#include "mpi-util.h"
#include "mpi-computation.h"

/** The programs arguments */
extern struct arguments args;

/** The total number of processes */
extern int size;

/** The currents processes rank */
extern int rank;

/** The associative operation to use for the prefix sums */
extern Type (*operation)(Type * e1, Type * e2);

/** The group of active processes */
MPI_Group active_group;

/** The commincator of the active processes */
MPI_Comm active_comm;

/** The number of active processes */
int active_size;

void run_computation(void)
{
	int i;
	int blocks  = compute_number_blocks(args.problem_size, size);
	int local_operations = 0, global_operations = 0;
	
	Type * problem = NULL;
	Type * copy	= NULL;
	
	subset_active_processes();
	
	if (rank < blocks) {
		
		for (i = 0; i < args.number_repetitions; i++) {
		
			MPI_Barrier(active_comm);
			
			if (rank == root) {
				problem = generate_random_problem(args.problem_size);
				copy    = copy_array(problem, args.problem_size);				
			}
	
			compute_problem(problem, args.problem_size);
	
			if (rank == root) {
				seq_benchmark_set_up();
				sequential_prefixsum(copy, args.problem_size, operation);
				seq_benchmark_tear_down();
				if (!compare_arrays(problem, copy, args.problem_size)) {
					bail_out(EXIT_FAILURE, "Error detected an incorrect result\n");
				}
				free(copy);
				free(problem);
			}
		}
		
		local_operations = get_plus_count();
		
		MPI_Reduce(&local_operations, &global_operations, 1, MPI_INT, MPI_SUM,
				root, active_comm);
		
		if (rank == root) {
			print_performance_summary(args.problem_size, args.number_cores,
				benchmark_avg_runtime(), benchmark_min_runtime(), 
				benchmark_max_runtime(), global_operations, 
				sequential_avg_runtime());
		}
		
		MPI_Comm_free(&active_comm);
		MPI_Group_free(&active_group);
	}
}

void compute_problem(Type * problem, int n) 
{
	int i;
	int blocks  = compute_number_blocks(n, size);
	int displ = 0;
	int * scount = (int *) malloc (blocks * sizeof(int));
	int * displs = (int *) malloc (blocks * sizeof(int));
	for (i = 0; i < blocks; i++) {
		scount[i] = block_size(i+1, n, size, blocks);
		displs[i] = displ;
		displ	  += scount[i];
	}

	Type * subproblem = malloc(scount[rank] * sizeof(Type));
	MPI_Datatype type = mpi_type();

	MPI_Scatterv(problem, scount, displs, type,
				 subproblem, scount[rank], type, root, active_comm);
	
	benchmark_set_up();
	
	prefixsum(subproblem, scount[rank], operation);
	
	benchmark_tear_down();
	
	MPI_Gatherv(subproblem, scount[rank], type, 
				problem, scount, displs, type, root, active_comm);
	
	free(scount);
	free(displs);
}

void subset_active_processes(void)
{
	MPI_Group global_group;
	
	MPI_Comm_group(MPI_COMM_WORLD, &global_group);
	
	int i;
	int blocs = compute_number_blocks(args.problem_size, size);
	int * ranks = (int *) malloc(blocs * sizeof(int));
	for (i = 0; i < blocs; i++) {
		ranks[i] = i;
	}
	
	MPI_Group_incl(global_group, blocs, ranks, &active_group);
	MPI_Comm_create(MPI_COMM_WORLD, active_group, &active_comm);
	
	if (rank < blocs) {
		MPI_Comm_size(active_comm, &active_size);
	}
	
	free(ranks);
}

int compute_number_blocks(int n, int p)
{
	int blocks;
	
	if (n >= p) blocks = p;
	else		blocks = n;	
	
	return blocks;
}

int block_size(int b, int n, int p, int blocs)
{
	int size;
	
	if (n > p) {
		if (b == p) size = n - (b-1)*(n/p);
		else 		size = n/p;
	} else {
		if (b <= n)	size = 1;
		else		size = 0;
	}
	
	return size;
}
