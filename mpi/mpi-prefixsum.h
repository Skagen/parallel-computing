#ifndef DEF_MPI_PREFIXSUM_H
#define DEF_MPI_PREFIXSUM_H

#include "matrix.h"

/**
 * Computes the f-prefix sums.
 *
 * @param x the array to compute the prefix sums on
 * @param n the number of prefix sums to compute
 * @param f the associative operation to use
 **/
void prefixsum(Type * x, int n, Type (*f)(Type * e1, Type * e2));

#endif
