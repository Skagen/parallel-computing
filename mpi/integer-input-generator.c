#include <stdlib.h>
#include "input-generator.h"
#include "mpi-util.h"

int * generate_random_problem(int size)
{
	int i;
	int *array = NULL;
	
	 array = (int *) malloc(size * sizeof(int));
	 if (array == NULL) {
	 	bail_out(EXIT_FAILURE, "failed to generate problem");
	 }
	 
	 for (i = 0; i < size; i++) {
	 	array[i] = rand() % (MAX_INT + 1);
	 }
	 
	 return array;
}
