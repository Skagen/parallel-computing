#include "comparator.h"
#include "matrix.h"

int compare_elements(Matrix * e1, Matrix * e2)
{
	int i, j;
	
	for (i = 0; i < MATRIX_SIZE; i++) {
		for (j = 0; j < MATRIX_SIZE; j++) {
			if (e1->a[i][j] != e2->a[i][j]) {
				return 0;
			}
		}
	}
	
	return 1;
}
