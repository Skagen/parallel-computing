#ifndef DEF_MPI_UTIL_H
#define DEF_MPI_UTIL_H

#include "matrix.h"

struct arguments {
	char * program_name;
	long problem_size;
	long number_cores;
	long number_repetitions;
};

/**
 * Computes the f-prefix sums on the first n elements of x.
 *
 * @param x the array to compute the prefix sums on
 * @param n the number of prefix sums to compute
 * @param f the operation to use for the prefix sums
 **/
void sequential_prefixsum(Type * x, long n, Type (*f)(Type * e1, Type * e2));

/**
 * Copies an array.
 *
 * @param x the array to copy
 * @param n the number of elements to copy
 * @return the copy of the n first elements of array
 **/
Type * copy_array(Type * x, long n);

/**
 * Prints the an integer array.
 *
 * @param x the array to print
 * @param n the number of first indices to print
 **/
void print_int_array(int *x, int n);

/**
 * Compares two arrays.
 *
 * @param x an array
 * @param y another array
 * @return 1 if the arrays are elementwise equal, 0 otherwise
 **/
int compare_arrays(Type * x, Type * y, long n);

/**
 * Prints a matrix array.
 *
 * @param x the array to print
 * @param n the number of first indices to print
 **/
void print_matrix_array(Matrix *x, int n);

/**
 * Prints a performance summary of the given data.
 *
 * @param problem_size the problem size
 * @param number_cores the max. number of processors used to solve the problem
 * @param avg_time the average time needed by the parallel solution to solve
 * 		  the problem
 * @param min_time the minimum time needed by the parallel solution to solve
 *		  the problem
 * @param min_time the maximum time needed by the parallel solution to solve
 *		  the problem
 * @param number_plus the number of plus operations needed to solve one problem
 * @param avg_seq_time the average time needed by the sequential algorithm to
 *		  solve the problem
 **/
void print_performance_summary(long problem_size, long number_cores, 
		double avg_time, double min_time, double max_time, long number_plus, 
		double avg_seq_time);
		
/**
 * Prints a message to stderr, then terminates the program.
 *
 * @param retval the programs exit value
 * @param fmt    the format of the error message
 * @param ...    arguments to build the error message
 **/
void bail_out(int retval, const char *fmt, ...);

#endif
