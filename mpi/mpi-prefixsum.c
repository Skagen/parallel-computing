#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include "mpi-util.h"
#include "mpi-prefixsum.h"
#include "mpi-computation.h"

extern int rank;
extern int active_size;
extern MPI_Comm active_comm;

/** The tag for the messages send from master to worker */
#define WORK_TAG 	1

/** The tag for the messages send from worker to master */
#define GATHER_TAG 	2

/**
 * Computes the prefix sums using at least n processors.
 * 
 * @param x the array to compute the prefix sums on
 * @param n the problem size, it must hold n >= active_size
 * @param f the associative operation used to compute the "sums"
 **/
void parallel_up_down_prefixsum(Type * x, int n, Type (*f)(Type * e1, Type *e2));

/**
 * Realizes the down phase of the up down parallel prefixsum algorithm.
 *
 * @param k  the k value that resulted from the up phase
 * @param kk the kk value that resulted from the up phase
 * @param x  the array to realize the up phase on
 * @param n  the size of the array, n >= active_size
 * @param f  the associative operation used to compute the "sums"
 **/
void down_phase(int k, int kk, Type * x, int n, Type (*f)(Type * e1, Type *e2));

/**
 * Realizes the up phase of the up down parallel prefixsum algorithm.
 *
 * @param x  the array to realize the up phase on
 * @param n  the size of the array, n >= active_size
 * @param f  the associative operation used to compute the "sums"
 * @output k  the k value that resulted from the up phase
 * @output kk the kk value that resulted from the up phase
 **/
void up_phase(Type * x, int n, Type (*f)(Type * e1, Type *e2), int * k, int * kk);

void prefixsum(Type * x, int n, Type (*f)(Type * e1, Type * e2))
{
	int i;
	Type sum;
	Type * y = NULL;
	
	sequential_prefixsum(x, n, f);
	
	sum = x[n - 1];
	
	if (rank == root) {
		y = (Type *) malloc((active_size + 1)* sizeof(Type));
	}
	
	MPI_Datatype type = mpi_type();
	MPI_Gather(&sum, 1, type, y + 1, 1, type, root, active_comm);
	
	parallel_up_down_prefixsum(y + 1,active_size,f);
	
	MPI_Scatter(y, 1, type, &sum, 1, type, root, active_comm);
	
	if (rank > 0) {
		for (i = 0; i < n; i++) {
			x[i] = f(&sum, &x[i]);
		}
	}
}

void parallel_up_down_prefixsum(Type * x, int n, Type (*f)(Type * e1, Type *e2))
{
	int k, kk;
	up_phase(x,n,f,&k,&kk);
	down_phase(k,kk,x,n,f);
}

void down_phase(int k, int kk, Type * x, int n, Type (*f)(Type * e1, Type *e2))
{	
	int i;
	Type x1, x2;
	MPI_Request send_request;
	MPI_Status recieve_status;

	for (k = k>>1; k > 1; k = kk) {
		kk = k >> 1;
		if (rank == root) {
			for (i = k - 1; i < n - kk; i += k) {
				MPI_Isend(&x[i], 1, mpi_type(), i/k, WORK_TAG, active_comm, 
						&send_request);
				MPI_Request_free(&send_request);
				MPI_Isend(&x[i+kk], 1, mpi_type(), i/k, WORK_TAG, active_comm, 
						&send_request);
				MPI_Request_free(&send_request);
			}
		}
		if (k - 1 + rank * k < n - kk) {
			MPI_Recv(&x1, 1, mpi_type(), root, WORK_TAG, active_comm, 
					&recieve_status);
			MPI_Recv(&x2, 1, mpi_type(), root, WORK_TAG, active_comm, 
					&recieve_status);
					
			x1 = f(&x1, &x2);
			
			MPI_Isend(&x1, 1, mpi_type(), root, GATHER_TAG, active_comm, 
					&send_request);
			MPI_Request_free(&send_request);
		}
		if (rank == root) {
			for (i = k - 1; i < n - kk; i += k) {
				MPI_Recv(&x[i+kk], 1, mpi_type(), i/k, GATHER_TAG, active_comm, 
						&recieve_status);
			}
		}
	}

}

void up_phase(Type * x, int n, Type (*f)(Type * e1, Type * e2), int * K, 
		int * KK)
{
	int i, k = 1, kk = 2;
	Type x1, x2;
	MPI_Request send_request;
	MPI_Status recieve_status;
	
	for (k = 1; k < n; k = kk) {
		kk = k << 1;
		if (rank == root) {
			for (i = kk - 1; i < n; i += kk) {
				MPI_Isend(&x[i-k], 1, mpi_type(), i/kk, WORK_TAG, active_comm,
						&send_request);
				MPI_Request_free(&send_request);
				MPI_Isend(&x[i], 1, mpi_type(), i/kk, WORK_TAG, active_comm,
						&send_request);
				MPI_Request_free(&send_request);
			}
		}
		if (kk - 1 + rank * kk < n) {
			MPI_Recv(&x1, 1, mpi_type(), root, WORK_TAG, active_comm, 
					&recieve_status);
			MPI_Recv(&x2, 1, mpi_type(), root, WORK_TAG, active_comm, 
					&recieve_status);
			x1 = f(&x1, &x2);
			MPI_Isend(&x1, 1, mpi_type(), root, GATHER_TAG, active_comm, 
					&send_request);
			MPI_Request_free(&send_request);
		}
		if (rank == root) {
			for (i = kk - 1; i < n; i += kk) {
				MPI_Recv(&x[i], 1, mpi_type(), i/kk, GATHER_TAG, active_comm, 
						&recieve_status);
			}
		}
	}
	*K  = k;
	*KK = kk;
}

