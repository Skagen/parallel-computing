#ifndef DEF_MPI_COMPUTATIONS_H
#define DEF_MPI_COMPUTATIONS_H

#include "matrix.h"

/** the rank of the root(master) process */
#define root 0

/** the group of the actual active processes */
extern MPI_Group active_group;

/** the communicator of the active processes */
extern MPI_Comm  active_comm;

/** Runs the computation process */
void run_computation(void);

/** Creates a subset of active processes */
void subset_active_processes(void);

/** 
 * Computes the solution for a problem (compr. distributing and gathering).
 * 
 * @param problem the proble to compute the solution for
 * @param n the problems size
 **/
void compute_problem(Type * problem, int n);

/**
 * Computes the number of blocks.
 *
 * @param n the problem size
 * @param p the number of processors to use
 * @return the number of blocs
 **/
int compute_number_blocks(int n, int p);

/**
 * Computes a blocs size.
 *
 * @param b the blocs index (starting by 1)
 * @param n the problem size
 * @param p the number of processors
 * @param blocs the total number of blocs
 * @return the size of bloc b
 **/
int block_size(int b, int n, int p, int blocs);

#endif
