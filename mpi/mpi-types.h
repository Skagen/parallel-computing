#ifndef DEF_MPI_TYPES_H
#define DEF_MPI_TYPES_H

#include <mpi.h>

#ifndef MATRIX_SIZE
	/** the value to use as width and height for the matrices */
	#define MATRIX_SIZE 1
#endif

/** @return a commited MPI description of the type specified at compile time */
MPI_Datatype mpi_type(void);

#include "matrix.h"

#endif
