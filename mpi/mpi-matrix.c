#include <stddef.h>
#include <mpi.h>
#include "matrix.h"
#include "mpi-types.h"

static MPI_Datatype type;

static int assigned = 0;

MPI_Datatype mpi_type(void)
{
	if (!assigned) {
		MPI_Datatype contiguous;
		MPI_Type_contiguous(MATRIX_SIZE, MPI_INT, &contiguous);
		MPI_Aint offsets[1]  = {offsetof(Matrix, a)};
		int bloclengths[] 	 = {MATRIX_SIZE};
		MPI_Datatype types[] = {contiguous};
		MPI_Type_struct(1, bloclengths, offsets, types, &type);
		MPI_Type_commit(&type);
	}
	
	assigned = 1;
	
	return type;
}
