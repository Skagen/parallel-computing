#include <stdlib.h>
#include "mpi-util.h"
#include "matrix.h"
#include "input-generator.h"

Matrix new_random_matrix(void);

Matrix * generate_random_problem(int size)
{
	int i;
	Matrix *problem = (Matrix *) malloc(size * sizeof(Matrix));
	
	if (problem == NULL) {
		bail_out(EXIT_FAILURE, "failed to generate input");
	}
	
	for (i = 0; i < size; i++) {
		problem[i] = new_random_matrix();
	}
	
	return problem;
}

Matrix new_random_matrix(void) {
	int i, j;
	Matrix matrix;
	for (i = 0; i < MATRIX_SIZE; i++) {
		for (j = 0; j < MATRIX_SIZE; j++) {
			matrix.a[i][j] = rand() % (2);
		}
	}
	return matrix;
}
