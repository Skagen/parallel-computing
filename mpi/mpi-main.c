#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <limits.h>

#include "mpi-computation.h"
#include "mpi-util.h"
#include "input-generator.h"

/**
 * Parses the programs arguments.
 *
 * @param argc the argument counter from the command line
 * @param argv the argument vector from the command line
 **/
void parse_arguments(int argc, char **argv);

/** Prints the programs usage. */
void usage(void);

/** The programs arguments */
extern struct arguments args;

int rank, size;

int main(int argc, char ** argv)
{	
	srand(time(NULL));
	
	MPI_Init(&argc, &argv);
	
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	parse_arguments(argc, argv);
	
	run_computation();

	MPI_Barrier(MPI_COMM_WORLD);
	
	MPI_Finalize();
	
	return 0;
}

void parse_arguments(int argc, char **argv)
{
	char *endptr = NULL;
	
	if (argc != 4) {
		usage();
		bail_out(EXIT_FAILURE, "invalid command line arguments");
	}
	
	args.problem_size = strtol(argv[1], &endptr, 10);
	
	if (args.problem_size == LONG_MIN || args.problem_size == LONG_MAX
			|| args.problem_size < 0) {
		bail_out(EXIT_FAILURE, "invalid problem size");
	}
	
	args.number_cores = strtol(argv[2], &endptr, 10);
	
	if (args.number_cores == LONG_MIN || args.number_cores == LONG_MAX
			|| args.number_cores < 0) {
		bail_out(EXIT_FAILURE, "invalid number of cores");
	}
	
	args.number_repetitions = strtol(argv[3], &endptr, 10);
	
	if (args.number_repetitions == LONG_MIN 
			|| args.number_repetitions == LONG_MAX 
			|| args.number_repetitions < 0) {
		bail_out(EXIT_FAILURE, "invalid number of repetitions");
	}
}

void usage(void) 
{
	(void) fprintf(stdout, "usage: %s <problem-size> <number-of-cores> "
			"<number-repetitions>\n", 
			args.program_name);
}
