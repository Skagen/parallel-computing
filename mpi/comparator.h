#ifndef DEF_COMPARATOR_H
#define DEF_COMPARATOR_H

#include "matrix.h"

/**
 * Compares the elements e1 and e2.
 *
 * @param e1 one of the elements to be compared
 * @param e2 the other of the elements to be compared
 * @return 1 if both elements are equal, 0 otherwise
 */
int compare_elements(Type * e1, Type * e2);

#endif
