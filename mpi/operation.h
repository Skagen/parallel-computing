#ifndef DEF_OPERATION_H
#define DEF_OPERATION_H

#include "mpi-types.h"

/**
 * An associative operation of the operands e1 and e2.
 * 
 * @param e1 the left operand
 * @param e2 the right operand
 * @return the result of the operation
 */
extern Type (*operation)(Type * e1, Type * e2);

/** @return the number of calls to &operation while the counters are enabled */
long get_plus_count(void);

/** Disables the counters of the calls to &operation */
void disable_counters(void);

#endif
