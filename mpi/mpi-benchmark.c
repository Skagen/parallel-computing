#include <mpi.h>
#include "operation.h"
#include "mpi-benchmark.h"

static double start_time = 0;
static double end_time   = 0;

static double max_time = 0;
static double min_time = 0;

static double sum_time = 0;

static int measurements = 0;

void benchmark_set_up(void)
{
	start_time = MPI_Wtime();
}

void benchmark_tear_down(void)
{
	end_time = MPI_Wtime();
	measurements++;
	sum_time += end_time - start_time;
	min_time = measurements == 1 ? (end_time - start_time) : min_time;
	min_time = min_time < (end_time - start_time) 
		? min_time 
		: (end_time - start_time);
	max_time = max_time > (end_time - start_time) 
		? max_time 
		: (end_time - start_time);
		
	disable_counters();
}

double benchmark_get_runtime(void)
{
	return end_time - start_time;
}

double benchmark_avg_runtime(void)
{
	return sum_time / measurements;
}

double benchmark_max_runtime(void)
{
	return max_time;
}
double benchmark_min_runtime(void)
{
	return min_time;
}

static double seq_start_time = 0;
static int seq_measurements = 0;
static double sum_seq_time = 0;

void seq_benchmark_set_up(void) 
{
	seq_start_time = MPI_Wtime();
}

void seq_benchmark_tear_down(void)
{
	sum_seq_time += (MPI_Wtime() - seq_start_time);
	seq_measurements++;
}

double sequential_avg_runtime(void)
{
	return sum_seq_time / seq_measurements;
}
