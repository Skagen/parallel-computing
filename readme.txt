Directories and files of this project:

	open-mp/
		contains the code of the open-mp project
		
	cilk/
		contains the code of the Cilk project
		
	mpi/
		contains the code of the MPI project
		
	plots/
		contains all plots, not only those which appear in the report
		
	benchmarking/
		contains scripts to run the benchmarks on the machines Saturn and Jupiter
		
	scripts/
		contains scripts to create plots or to manage the project
		
	measures/
		contains the data issued by the benchmarks. The directroy contains 
		also the results that were realized with wrong parameters, which were
		therefore not discusses in the report.
		
	report.pdf
		the project report
		
	readme.txt
		this description
		
Compiling the implementations:
	
	(1) cd <cilk|open-mp|mpi>
	
	(2) make <target> TYPE=<int|Matrix> [MATRIX_SIZE=<m>] [COUNTERS=enable]
	
Running the programs:

	Open-MP and Cilk programs:
	
		(1) ./program <problem-size> <number-processors> <repetitions>
		
	MPI programs:
		
		(1) mpirun <mpi-options> ./program <problem-size> <number-processors> <repetitions>
		
	Program output:
		<problem-size> <number-processors> <avg-parallel-time> <min-parallel-time> <max-parallel-time> <work> <avg-sequential-time>
		
Running the benchmarks:

	(1) upload project to host
	
	(2) connect to host
	
	(3) cd <project-path>/benchmarking/
	
	(4) ./benchmark.sh <saturn|jupiter> <open-mp|cilk|mpi> <program-name> <counters|problems|cores>
	
Plotting the results:

	(1) cd <project-path>/scripts/
	
	Plotting counters:
	
		(2) Rscript plot-counters.r <path-to-counter-data>	
	
	Plotting the speedup:
	
		(2) Rscript plot-speedup-graph.r <as-f-cores|as-f-problems> <path-to-matrix-data> <path-to-integer-data>
