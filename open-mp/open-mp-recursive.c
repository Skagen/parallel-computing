#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#include "util.h"
#include "prefixsum.h"
#include "operation.h"

extern struct arguments args;

#define NUM_PROCESSORS args.number_cores

void prefixsum(Type *x, int n, Type (*f)(Type *e1, Type *e2)) {
	int i;
	Type * y = malloc((n/2) * sizeof(Type));
	
	if (n <= 1) return;
	
	if (y == NULL) {
		bail_out(EXIT_FAILURE, "failed to allocate memory");
	}
	
	#pragma omp parallel for num_threads(NUM_PROCESSORS) shared(x) private(i)
	for (i = 0; i < n / 2; i++) {
		y[i] = f(&x[2*i],&x[2*i + 1]);
	} /* implicit barrier */
	
	prefixsum(y, n/2, f);
	
	x[1] = y[0];
	#pragma omp parallel for num_threads(NUM_PROCESSORS) shared(x) private(i)
	for (i = 1; i < n / 2; i++) {
		x[2*i] = f(&y[i-1], &x[2*i]);
		x[2*i + 1] = y[i];
	} /* implicit barrier */
	
	if (n % 2 == 1) x[n-1] = f(&y[n/2 - 1],&x[n-1]);
	
	free(y);
}
