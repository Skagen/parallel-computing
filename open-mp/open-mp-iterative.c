#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#include "util.h"
#include "prefixsum.h"
#include "operation.h"

extern struct arguments args;

#define NUM_PROCESSORS args.number_cores

void prefixsum(Type *x, int n, Type (*f)(Type *e1, Type *e2)) {
	int k, kk, i;
	for (k = 1; k < n; k = kk) {
		kk = k << 1;
		#pragma omp parallel for num_threads(NUM_PROCESSORS) shared(x) private(i)
		for (i = kk - 1; i < n; i += kk) {
			x[i] = f(&x[i-k],&x[i]);
		} /* implicit barrier */
	}
	for (k = k>>1; k > 1; k = kk) {
		kk = k >> 1;
		#pragma omp parallel for num_threads(NUM_PROCESSORS) shared(x) private(i)
		for (i = k - 1; i < n - kk; i += k) {
			x[i+kk] = f(&x[i], &x[i+kk]);
		} /* implicit barrier */
	}
}
