#ifndef INPUT_GENERATOR_H
#define INPUT_GENERATOR_H

#include "matrix.h"

/** The maximum integer value to use for the integer problem */
#define MAX_INT 15

/**
 * Generates a new random problem.
 *
 * @param size the size of the problem to generate
 * @return a new random problem of size size
 **/
Type * generate_random_problem(int size);

#endif
