#include <omp.h>
#include "operation.h"
#include "matrix.h"

static long plus_counter = 0;
static int disable = 0;

static omp_lock_t lock;

Matrix boolean_matrix_multiplication(Matrix *a, Matrix *b);

Matrix (*operation)(Matrix *a, Matrix * b) = &boolean_matrix_multiplication;

Matrix boolean_matrix_multiplication(Matrix *a, Matrix *b)
{
	Matrix c;
	int i, j, k;
	
	#ifdef ENABLE_COUNTERS
	if (!disable) {
		omp_set_lock(&lock);
		plus_counter++;
		omp_unset_lock(&lock);
	}
	#endif
	
	for (i = 0; i < MATRIX_SIZE; i++) {
		for (j = 0; j < MATRIX_SIZE; j++) {
			c.a[i][j] = 0;
			for (k = 0; k < MATRIX_SIZE; k++) {
				c.a[i][j] = c.a[i][j] || (a->a[i][k] && b->a[k][j]);
			}
		}
	}
	
	return c;
}

long get_plus_count(void) 
{
	return plus_counter;
}

void disable_counters(void)
{
	disable = 1;
}

void init_counters(void)
{
	omp_init_lock(&lock);
}
