#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#include "util.h"
#include "prefixsum.h"
#include "operation.h"

extern struct arguments args;

#define NUM_PROCESSORS args.number_cores

void prefixsum(Type *x, int n, Type (*f)(Type *e1, Type *e2)) {
	Type * z = x;
	Type * t;
	Type *y, *y0;
	int k, i;
	
	y = y0 = (Type *) malloc(n * sizeof(Type));
	
	for (k = 1; k < n; k=k<<1) {
		#pragma omp parallel for num_threads(NUM_PROCESSORS) \
								 shared(x,y,z) private(i)
		for (i = 0; i < n; i++) {
			y[i] = (i < k) ? z[i] : f(&z[i-k],&z[i]); 
		}
		t = z; z = y; y = t; 
	}
	if (x != z) {
		#pragma omp parallel for num_threads(NUM_PROCESSORS) \
								 shared(x,z) private(i)
		for (i = k>>1; i < n; i++) {
			x[i] = z[i];
		}
	}
	
	free(y0);
}
