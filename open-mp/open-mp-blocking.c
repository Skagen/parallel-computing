#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#include "util.h"
#include "prefixsum.h"

extern struct arguments args;

#define NUM_PROCESSORS args.number_cores
#define P args.number_cores

void parallel_prefixsum(Type *x, int n, Type (*f)(Type *e1, Type *e2));
void scan_seq(Type * x, int n, Type (*f)(Type* e1, Type* e2));

int compute_number_blocks(int n, int p);
int block_size(int b, int n, int p);
int block_displ(int b, int n, int p);

void prefixsum(Type *x, int n, Type (*f)(Type *e1, Type *e2)) 
{
	int b = compute_number_blocks(n, P);
	int j, i, w;
	Type *y = (Type *) malloc(b * sizeof(Type));
	
	#pragma omp parallel for num_threads(NUM_PROCESSORS) \
								 shared(x,y) private(i,w)
	for (i = 0; i < b; i++) {
		w = block_size(i+1, n, P);
		scan_seq(&x[block_displ(i+1, n, P)], w, f);
		y[i] = x[block_displ(i+1, n, P) + w - 1];
	}
	
	parallel_prefixsum(y, b, f);
	
	#pragma omp parallel for num_threads(NUM_PROCESSORS) \
								 shared(x,y) private(i,j,w)
	for (i = 1; i < b; i++) {
		w = block_size(i+1, n, P);
		for (j = 0; j < w; j++) {
			x[block_displ(i+1, n, P) + j] = f(&y[i-1],&x[block_displ(i+1, n, P) + j]);
		}
	}
	
	free(y);
}

void parallel_prefixsum(Type *x, int n, Type (*f)(Type *e1, Type *e2))
{
	int k, kk, i;
	for (k = 1; k < n; k = kk) {
		kk = k << 1;
		#pragma omp parallel for num_threads(NUM_PROCESSORS) shared(x) private(i)
		for (i = kk - 1; i < n; i += kk) {
			x[i] = f(&x[i-k],&x[i]);
		} /* implicit barrier */
	}
	for (k = k>>1; k > 1; k = kk) {
		kk = k >> 1;
		#pragma omp parallel for num_threads(NUM_PROCESSORS) shared(x) private(i)
		for (i = k - 1; i < n - kk; i += k) {
			x[i+kk] = f(&x[i], &x[i+kk]);
		} /* implicit barrier */
	}
}

void scan_seq(Type * x, int n, Type (*f)(Type* e1, Type* e2)) 
{	
	Type sum;
	register int i;
	
	if (n == 0) return;
		
	sum = x[0];
	
	for (i = 1; i < n; i++) {
		sum = f(&sum,&x[i]);
		x[i] = sum;
	}
}

int compute_number_blocks(int n, int p)
{
	int blocks;
	
	if (n >= p) blocks = p;
	else		blocks = n;	
	
	return blocks;
}

int block_size(int b, int n, int p)
{
	int size;
	
	if (n > p) {
		if (b == p) size = n - (b-1)*(n/p);
		else 		size = n/p;
	} else {
		if (b <= n)	size = 1;
		else		size = 0;
	}
	
	return size;
}

int block_displ(int b, int n, int p)
{
	int displ;
	
	if (n > p)
		displ = (b - 1) * (n/p);
	else 
		displ = b - 1;
		
	return displ;
}
